from kivy.uix.image import Image as ImageWidget
from kivy.properties import (
    NumericProperty,
    AliasProperty,
    StringProperty,
    ListProperty,
)
from kivy.metrics import dp

class Bird(ImageWidget):
    accel_fall = NumericProperty(0.45)
    accel_jump = NumericProperty(7)
    speed = NumericProperty(0)
    angle = AliasProperty(lambda self: 5 * self.speed, None, bind=["speed"])
    current_image = NumericProperty(0)
    images = ListProperty()
    image = StringProperty()

    def __init__(self, **kwargs):
        super(Bird, self).__init__(**kwargs)
        self.images = [
            "images/bird/frame-1.png",
            "images/bird/frame-2.png",
            "images/bird/frame-3.png",
            "images/bird/frame-4.png",
            "images/bird/frame-5.png",
            "images/bird/frame-6.png",
            "images/bird/frame-7.png",
            "images/bird/frame-8.png",
        ]

    def gravity_on(self, height):
        self.pos_hint.pop("center_y", None)
        self.center_y = 0.6 * height

    def bump(self):
        self.speed = self.accel_jump
        
    def gravity_off(self):
        self.accel_fall = 0
        self.accel_jump = 0
        self.speed = 0

    def update(self, nap):
        self.speed -= self.accel_fall
        self.y += self.speed
        self.current_image = (self.current_image + 1) % 8
        self.image = self.images[self.current_image]


class Coin(ImageWidget):
    speed = NumericProperty(8)
    current_image = NumericProperty(0)
    images = ListProperty()
    image = StringProperty()

    def __init__(self, **kwargs):
        super(Coin, self).__init__(**kwargs)
        self.images = [
            "images/coin/frame-1.png",
            "images/coin/frame-2.png",
            "images/coin/frame-3.png",
            "images/coin/frame-4.png",
            "images/coin/frame-5.png",
            "images/coin/frame-6.png",
            "images/coin/frame-7.png",
            "images/coin/frame-8.png",
            "images/coin/frame-9.png",
            "images/coin/frame-10.png",
            "images/coin/frame-11.png",
            "images/coin/frame-12.png",
            "images/coin/frame-13.png",
            "images/coin/frame-14.png",
            "images/coin/frame-15.png",
            "images/coin/frame-16.png",
            "images/coin/frame-17.png",
            "images/coin/frame-18.png",
            "images/coin/frame-19.png",
            "images/coin/frame-20.png",
            "images/coin/frame-21.png",
            "images/coin/frame-22.png",
            "images/coin/frame-23.png",
            "images/coin/frame-24.png",
        ]

    def update(self, nap):
        self.x -= self.speed
        self.current_image = (self.current_image + 1) % 24
        self.image = self.images[self.current_image]


class Plane(ImageWidget):
    speed = NumericProperty(16)
    current_image = NumericProperty(0)
    images = ListProperty()
    image = StringProperty()

    def __init__(self, **kwargs):
        super(Plane, self).__init__(**kwargs)
        self.images = [
            "images/plane/frame-1.png",
            "images/plane/frame-2.png",
        ]

    def update(self, nap):
        self.x -= self.speed
        self.current_image = (self.current_image + 1) % 2
        self.image = self.images[self.current_image]


class Explosion(ImageWidget):
    image = StringProperty("")
    images_list = ListProperty()
    speed = NumericProperty(16)

    def __init__(self, x, y, type_):
        super(Explosion, self).__init__()
        self.x = x
        self.y = y
        self.type_ = type_
        if self.type_ == "yellow":
            for i in range(11):
                self.image = f"images/explosion/yellow/{i+1}.png"
                self.images_list.append(self.image)
        
        elif self.type_ == "green":
            for i in range(17):
                self.image = f"images/explosion/green/{i+1}.png"
                self.images_list.append(self.image)
            
        elif self.type_ == "purple":
            for i in range(14):
                self.image = f"images/explosion/purple/{i+1}.png"
                self.images_list.append(self.image)
            self.size = (dp(140), dp(140))

    def update(self, nap):
        self.x -= self.speed
