# :yellow_heart: :blue_heart: COINBIRD :blue_heart: :yellow_heart:
## Game App for Android using Python with Kivy & KivyMD.
Coinbird very likes coins. Help it collects 1000 coins in one attempt.

Watch gameplay on [YouTube](https://youtu.be/sWBgjQCnRPo)

# Prerequisites
- [Python 3.6+](https://www.python.org/)
- [Kivy>=2.0.0](https://kivy.org/#home)
- [KivyMD>=0.104.2](https://kivymd.readthedocs.io/en/latest/)
