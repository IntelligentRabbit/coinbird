from kivy.uix.screenmanager import Screen
from kivymd.uix.card import MDCard
from kivy.uix.boxlayout import BoxLayout
from kivymd.uix.button import MDFlatButton
from kivymd.uix.dialog import MDDialog
from kivy.properties import StringProperty, BooleanProperty
from kivy.animation import Animation
from kivy.metrics import sp

from io_data import save, load


class ShopCard(MDCard):
    image = StringProperty()
    description = StringProperty()
    btn_text = StringProperty()
    bought = BooleanProperty(False)    


class ShopScreen(Screen):
    shopcards = []
    dialog = None

    shopcards_descriptions = {
        "shopcard1": [
            "Double health",
            100,
            False,
        ],
        "shopcard2": [
            "Triple health",
            200,
            False,
        ],
        "shopcard3": [
            "Lower gravity",
            300,
            False,
        ],
        "shopcard4": [
            "Strength",
            300,
            False,
        ],
        "shopcard5": [
            "Dragon health",
            999,
            False,
        ],
        "shopcard6": [
            "Goblet of Rabbit",
            1000,
            False,
        ],
    }

    def on_enter(self):
        self.animate_title_label()
        self.ids.carousel.clear_widgets()
        self.load_shop_saves()
        self.generate_and_added_slides_to_carousel()
        
    def load_shop_saves(self):
        saves = load("saves_shop")
        if len(saves) != 0:
            self.shopcards_descriptions["shopcard1"][2] = saves["shopcard1_bought"]
            self.shopcards_descriptions["shopcard2"][2] = saves["shopcard2_bought"]
            self.shopcards_descriptions["shopcard3"][2] = saves["shopcard3_bought"]
            self.shopcards_descriptions["shopcard4"][2] = saves["shopcard4_bought"]
            self.shopcards_descriptions["shopcard5"][2] = saves["shopcard5_bought"]
            self.shopcards_descriptions["shopcard6"][2] = saves["shopcard6_bought"]

    def generate_and_added_slides_to_carousel(self):
        for shopcard in self.shopcards_descriptions.keys():
            card = ShopCard(
                image=f"images/shop/{shopcard}.png",
                description=self.shopcards_descriptions.get(shopcard)[0],
                btn_text=f"{self.shopcards_descriptions.get(shopcard)[1]}",
                bought=self.shopcards_descriptions.get(shopcard)[2],
            )
            self.shopcards.append(card)
            self.ids.carousel.add_widget(card)

    def shop_manage(self):
        self.game.sound_click.play()
        shopcard_num = self.ids.carousel.index
        if shopcard_num == 0:
            if self.game.player_coins >= self.shopcards_descriptions["shopcard1"][1]:
                self.game.max_player_hp = 2
                self.game.player_coins -= self.shopcards_descriptions["shopcard1"][1]
                save(
                    "saves",
                    max_player_hp=self.game.max_player_hp,
                    player_coins=self.game.player_coins,
                    bird_accel_fall=self.game.bird_accel_fall,
                    bird_accel_jump=self.game.bird_accel_jump,
                )
                self.shopcards_descriptions["shopcard1"][2] = True
                self.shopcards[0].bought = True
                save(
                    "saves_shop",
                    shopcard1_bought=self.shopcards_descriptions["shopcard1"][2],
                    shopcard2_bought=self.shopcards_descriptions["shopcard2"][2],
                    shopcard3_bought=self.shopcards_descriptions["shopcard3"][2],
                    shopcard4_bought=self.shopcards_descriptions["shopcard4"][2],
                    shopcard5_bought=self.shopcards_descriptions["shopcard5"][2],
                    shopcard6_bought=self.shopcards_descriptions["shopcard6"][2],
                )

        if shopcard_num == 1:
            if (
                self.game.player_coins >= self.shopcards_descriptions["shopcard2"][1]
                and self.game.max_player_hp == 2
            ):
                self.game.max_player_hp = 3
                self.game.player_coins -= self.shopcards_descriptions["shopcard2"][1]
                save(
                    "saves",
                    max_player_hp=self.game.max_player_hp,
                    player_coins=self.game.player_coins,
                    bird_accel_fall=self.game.bird_accel_fall,
                    bird_accel_jump=self.game.bird_accel_jump,
                )
                self.shopcards_descriptions["shopcard2"][2] = True
                self.shopcards[1].bought = True
                save(
                    "saves_shop",
                    shopcard1_bought=self.shopcards_descriptions["shopcard1"][2],
                    shopcard2_bought=self.shopcards_descriptions["shopcard2"][2],
                    shopcard3_bought=self.shopcards_descriptions["shopcard3"][2],
                    shopcard4_bought=self.shopcards_descriptions["shopcard4"][2],
                    shopcard5_bought=self.shopcards_descriptions["shopcard5"][2],
                    shopcard6_bought=self.shopcards_descriptions["shopcard6"][2],
                )

        if shopcard_num == 2:
            if self.game.player_coins >= self.shopcards_descriptions["shopcard3"][1]:
                self.game.bird_accel_fall = 0.3
                self.game.player_coins -= self.shopcards_descriptions["shopcard3"][1]
                save(
                    "saves",
                    max_player_hp=self.game.max_player_hp,
                    player_coins=self.game.player_coins,
                    bird_accel_fall=self.game.bird_accel_fall,
                    bird_accel_jump=self.game.bird_accel_jump,
                )
                self.shopcards_descriptions["shopcard3"][2] = True
                self.shopcards[2].bought = True
                save(
                    "saves_shop",
                    shopcard1_bought=self.shopcards_descriptions["shopcard1"][2],
                    shopcard2_bought=self.shopcards_descriptions["shopcard2"][2],
                    shopcard3_bought=self.shopcards_descriptions["shopcard3"][2],
                    shopcard4_bought=self.shopcards_descriptions["shopcard4"][2],
                    shopcard5_bought=self.shopcards_descriptions["shopcard5"][2],
                    shopcard6_bought=self.shopcards_descriptions["shopcard6"][2],
                )

        if shopcard_num == 3:
            if self.game.player_coins >= self.shopcards_descriptions["shopcard4"][1]:
                self.game.bird_accel_jump = 8
                self.game.player_coins -= self.shopcards_descriptions["shopcard4"][1]
                save(
                    "saves",
                    max_player_hp=self.game.max_player_hp,
                    player_coins=self.game.player_coins,
                    bird_accel_fall=self.game.bird_accel_fall,
                    bird_accel_jump=self.game.bird_accel_jump,
                )
                self.shopcards_descriptions["shopcard4"][2] = True
                self.shopcards[3].bought = True
                save(
                    "saves_shop",
                    shopcard1_bought=self.shopcards_descriptions["shopcard1"][2],
                    shopcard2_bought=self.shopcards_descriptions["shopcard2"][2],
                    shopcard3_bought=self.shopcards_descriptions["shopcard3"][2],
                    shopcard4_bought=self.shopcards_descriptions["shopcard4"][2],
                    shopcard5_bought=self.shopcards_descriptions["shopcard5"][2],
                    shopcard6_bought=self.shopcards_descriptions["shopcard6"][2],
                )

        if shopcard_num == 4:
            if self.game.player_coins >= self.shopcards_descriptions["shopcard5"][1]:
                self.game.max_player_hp = 1000000
                self.game.player_coins -= self.shopcards_descriptions["shopcard5"][1]
                save(
                    "saves",
                    max_player_hp=self.game.max_player_hp,
                    player_coins=self.game.player_coins,
                    bird_accel_fall=self.game.bird_accel_fall,
                    bird_accel_jump=self.game.bird_accel_jump,
                )
                self.shopcards_descriptions["shopcard5"][2] = True
                self.shopcards[4].bought = True
                save(
                    "saves_shop",
                    shopcard1_bought=self.shopcards_descriptions["shopcard1"][2],
                    shopcard2_bought=self.shopcards_descriptions["shopcard2"][2],
                    shopcard3_bought=self.shopcards_descriptions["shopcard3"][2],
                    shopcard4_bought=self.shopcards_descriptions["shopcard4"][2],
                    shopcard5_bought=self.shopcards_descriptions["shopcard5"][2],
                    shopcard6_bought=self.shopcards_descriptions["shopcard6"][2],
                )
        if shopcard_num == 5:
            if self.game.player_coins >= self.shopcards_descriptions["shopcard6"][1]:
                self.game.player_coins -= self.shopcards_descriptions["shopcard6"][1]
                save(
                    "saves",
                    max_player_hp=self.game.max_player_hp,
                    player_coins=self.game.player_coins,
                    bird_accel_fall=self.game.bird_accel_fall,
                    bird_accel_jump=self.game.bird_accel_jump,
                )
                self.shopcards_descriptions["shopcard6"][2] = True
                self.shopcards[5].bought = True
                save(
                    "saves_shop",
                    shopcard1_bought=self.shopcards_descriptions["shopcard1"][2],
                    shopcard2_bought=self.shopcards_descriptions["shopcard2"][2],
                    shopcard3_bought=self.shopcards_descriptions["shopcard3"][2],
                    shopcard4_bought=self.shopcards_descriptions["shopcard4"][2],
                    shopcard5_bought=self.shopcards_descriptions["shopcard5"][2],
                    shopcard6_bought=self.shopcards_descriptions["shopcard6"][2],
                )
                self.show_dialog()
                
    def return_btn_press(self):
        self.game.current = "welcome_screen"
        self.game.sound_click.play()

    def animate_title_label(self):
        anim = Animation(font_size=sp(24), duration=0.2)
        anim += Animation(font_size=sp(22), duration=0.2)
        anim.start(self.ids.title_label)

    def show_dialog(self):
        if not self.dialog:
            self.dialog = MDDialog(
                auto_dismiss=False,
                type="custom",
                content_cls=RabbitDialog(),
                radius=[20, 20, 20, 20],
                buttons=[
                    MDFlatButton(
                        text="CLOSE",
                        font_name="fonts/Komika_display.ttf",
                        on_release=lambda _: self.press_close_btn(),
                    ),
                ],
            )
        self.dialog.open()

    def press_close_btn(self):
        self.game.sound_click.play()
        self.dialog.dismiss()
        
        
class RabbitDialog(BoxLayout):
    pass
        