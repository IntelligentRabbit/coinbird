from kivy.uix.screenmanager import Screen
from kivy.animation import Animation
from kivy.metrics import sp

from io_data import save


class OptionsScreen(Screen):
    def on_enter(self):
        self.animate_title_label()
        
    def sound_btn_press(self):
        self.game.sound_on = not self.game.sound_on
        self.game.sound_mute_unmute()
        save(
            "saves_setting",
            music_on=self.game.music_on,
            sound_on=self.game.sound_on,
        )
        self.game.sound_click.play()

    def music_btn_press(self):
        self.game.music_on = not self.game.music_on
        self.game.music_mute_unmute()
        save(
            "saves_setting",
            music_on=self.game.music_on,
            sound_on=self.game.sound_on,
        )
        self.game.sound_click.play()

    def return_btn_press(self):
        self.game.current = "welcome_screen"
        self.game.sound_click.play()

    def animate_title_label(self):
        anim = Animation(font_size=sp(24), duration=0.2)
        anim += Animation(font_size=sp(22), duration=0.2)
        anim.start(self.ids.title_label)
        