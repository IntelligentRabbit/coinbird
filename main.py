from random import randint
from functools import partial

from kivymd.app import MDApp
from kivymd.uix.boxlayout import MDBoxLayout
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from kivy.properties import (
    NumericProperty,
    BooleanProperty,
    StringProperty,
    ObjectProperty,
)
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.core.audio import SoundLoader
from kivy.core.text import LabelBase

from kivy.animation import Animation
from kivy.metrics import sp

from background import Background
from objects import Bird, Coin, Plane, Explosion
from io_data import save, load
from welcome_screen import WelcomeScreen
from options_screen import OptionsScreen
from shop_screen import ShopScreen

VERSION = "v. 1.0.2"
Builder.load_file("objects.kv")
Builder.load_file("welcome_screen.kv")
Builder.load_file("options_screen.kv")
Builder.load_file("shop_screen.kv")


class Game(ScreenManager):
    current_player_coins = NumericProperty(0)
    player_coins = NumericProperty(0)
    music_on = BooleanProperty(True)
    sound_on = BooleanProperty(True)
    player_hp = NumericProperty(1)
    max_player_hp = NumericProperty(1)
    bird_accel_fall = NumericProperty(0.45)
    bird_accel_jump = NumericProperty(7)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.main_theme = SoundLoader.load("sounds/duckwalk.ogg")
        self.sound_notification = SoundLoader.load("sounds/game-notification.wav")
        self.sound_plane = SoundLoader.load("sounds/propeller-engine.wav")
        self.sound_click = SoundLoader.load("sounds/btn_click.wav")
        self.sound_gameover = SoundLoader.load("sounds/losing-bleeps.wav")
        self.sound_hit = SoundLoader.load("sounds/get_hit.wav")
        self.sound_game_win = SoundLoader.load("sounds/game_win.ogg")
        self.sound_fireworks = SoundLoader.load("sounds/fireworks-bang-in-sky.wav")
        
        self.main_theme.loop = True
        self.sound_fireworks.loop = True
        self.sound_game_win.volume = .6
        
        self.sound_list = [
            self.sound_notification,
            self.sound_click,
            self.sound_plane,
            self.sound_gameover,
            self.sound_hit,
            self.sound_game_win,
            self.sound_fireworks,
        ]
        self.music_list = [
            self.main_theme,
        ]

        saves_setting = load("saves_setting")
        if len(saves_setting) != 0:
            self.music_on = saves_setting["music_on"]
            self.sound_on = saves_setting["sound_on"]

        saves = load("saves")
        if len(saves) != 0:
            self.max_player_hp = saves["max_player_hp"]
            self.player_coins = saves["player_coins"]
            self.bird_accel_fall = saves["bird_accel_fall"]
            self.bird_accel_jump = saves["bird_accel_jump"]

        self.sound_mute_unmute()
        self.music_mute_unmute()
        self.main_theme.play()

    def sound_mute_unmute(self):
        if self.sound_on:
            for sound in self.sound_list:
                sound.volume = 1
        else:
            for sound in self.sound_list:
                sound.volume = 0

    def music_mute_unmute(self):
        if self.music_on:
            for music in self.music_list:
                music.volume = 1
        else:
            for music in self.music_list:
                music.volume = 0


class GameScreen(Screen):
    game_widget = ObjectProperty(None)

    def on_pre_enter(self):
        self.game_widget.start()

    def on_leave(self):
        self.game_widget.reset()


class GameWidget(Widget):
    coins = []
    planes = []
    explosions = []
    events = []
    victory = BooleanProperty(False)

    def start(self):
        self.bg = self.ids.background
        self.bird = self.ids.bird_id
        self.bird.accel_fall = CoinbirdApp.game.bird_accel_fall
        self.bird.accel_jump = CoinbirdApp.game.bird_accel_jump
        event = Clock.schedule_interval(self.update, 1 / 24)
        event2 = Clock.schedule_interval(self.spawn_coins, 4)
        event3 = Clock.schedule_interval(self.spawn_planes, 10)
        self.events = [event, event2, event3]
        self.bird.gravity_on(self.height)
        self.on_touch_down = self.user_action
        CoinbirdApp.game.player_hp = CoinbirdApp.game.max_player_hp

    def update(self, nap):
        self.bg.update(nap)
        self.bird.update(nap)

        for coin in self.coins:
            coin.update(nap)
            if coin.x <= -coin.width:
                self.remove_widget(coin)
                self.coins.remove(coin)

        for plane in self.planes:
            plane.update(nap)
            if plane.x <= -plane.width:
                self.remove_widget(plane)
                self.planes.remove(plane)
                CoinbirdApp.game.sound_plane.stop()

        for exp in self.explosions:
            exp.update(nap)
            if exp.x <= -exp.width:
                self.remove_widget(exp)
                self.explosions.remove(exp)

        self.check_collisions()
        if self.test_game_over():
            self.game_over()
        if self.test_game_win():
            self.animate_victory()
            
    def user_action(self, *args):
        self.bird.bump()

    def spawn_coins(self, dt):
        spawn_y = randint(self.y + 460, self.height - 200)
        spawn_x = self.width
        coin = Coin(x=spawn_x, y=spawn_y)
        self.add_widget(coin)
        self.coins.append(coin)

    def spawn_planes(self, dt):
        spawn_y = randint(self.y + 460, self.height - 200)
        spawn_x = self.width
        plane = Plane(x=spawn_x, y=spawn_y)
        self.add_widget(plane)
        self.planes.append(plane)
        CoinbirdApp.game.sound_plane.play()

    def get_explosion(self, exp_x, exp_y, exp_type):
        exp = Explosion(exp_x, exp_y, exp_type)
        self.add_widget(exp)
        self.explosions.append(exp)

        def change_explosion(img, dt):
            exp.image = exp.images_list[img]
            
        if exp_type == "yellow":
            for i in range(11):
                Clock.schedule_once(partial(change_explosion, i), (0.2 + i * 0.06))
        elif exp_type == "green":
            for i in range(17):
                Clock.schedule_once(partial(change_explosion, i), (0.2 + i * 0.06))
        elif exp_type == "purple":
            for i in range(14):
                Clock.schedule_once(partial(change_explosion, i), (0.2 + i * 0.06))

    def get_fireworks(self, dt):
        self.get_explosion(int(self.width * 0.3), int(self.height * 0.8), "green")
        self.get_explosion(int(self.width * 0.6), int(self.height * 0.85), "yellow")
        self.get_explosion(int(self.width * 0.9), int(self.height * 0.8), "purple")
        
    def check_collisions(self):
        for coin in self.coins:
            if coin.collide_widget(self.bird):
                self.remove_widget(coin)
                self.coins.remove(coin)
                CoinbirdApp.game.sound_notification.play()
                CoinbirdApp.game.current_player_coins += 1

        for plane in self.planes:
            if plane.collide_widget(self.bird):
                self.get_explosion(plane.x, plane.y, "green")
                CoinbirdApp.game.sound_plane.stop()
                self.remove_widget(plane)
                self.planes.remove(plane)
                CoinbirdApp.game.player_hp -= 1
                if CoinbirdApp.game.player_hp != 0:
                    CoinbirdApp.game.sound_hit.play()

    def animate_victory_label(self):
        anim = Animation(opacity=1, duration=0.5)
        anim &= Animation(font_size=sp(26), duration=0.5)
        anim += Animation(opacity=.99, duration=8)
        anim.bind(on_complete=lambda a, w: self.game_win())
        anim.start(self.ids.victory_label)

    def animate_victory(self):
        self.victory = True
        self.events[1].cancel()
        self.events[2].cancel()
        self.bird.gravity_off()
        self.animate_victory_label()
        event = Clock.schedule_interval(self.get_fireworks, 1)
        self.events.append(event)
        CoinbirdApp.game.sound_game_win.play()
        CoinbirdApp.game.sound_fireworks.play()
                    
    def test_game_over(self):
        if (
            self.bird.y < 96
            or self.bird.y > self.height - self.bird.height
            or CoinbirdApp.game.player_hp == 0
        ):
            return True
        return False

    def test_game_win(self):
        if (
            CoinbirdApp.game.current_player_coins == 1000
            and not self.victory
        ):
            return True
        return False
        
    def game_over(self):
        for event in self.events:
            event.cancel()
        CoinbirdApp.game.player_coins += CoinbirdApp.game.current_player_coins
        save(
            "saves",
            max_player_hp=CoinbirdApp.game.max_player_hp,
            player_coins=CoinbirdApp.game.player_coins,
            bird_accel_fall=CoinbirdApp.game.bird_accel_fall,
            bird_accel_jump=CoinbirdApp.game.bird_accel_jump,
        )
        CoinbirdApp.game.sound_gameover.play()
        CoinbirdApp.game.current = "welcome_screen"

    def game_win(self):
        for event in self.events:
            event.cancel()
        CoinbirdApp.game.player_coins += CoinbirdApp.game.current_player_coins
        save(
            "saves",
            max_player_hp=CoinbirdApp.game.max_player_hp,
            player_coins=CoinbirdApp.game.player_coins,
            bird_accel_fall=CoinbirdApp.game.bird_accel_fall,
            bird_accel_jump=CoinbirdApp.game.bird_accel_jump,
        )
        CoinbirdApp.game.sound_fireworks.stop()
        CoinbirdApp.game.current = "welcome_screen"
        
    def reset(self):
        for coin in self.coins:
            self.remove_widget(coin)
            self.coins.remove(coin)
        for plane in self.planes:
            self.remove_widget(plane)
            self.planes.remove(plane)
        CoinbirdApp.game.current_player_coins = 0
        self.bird.speed = 0
        self.victory = False
        self.ids.victory_label.font_size = sp(10)
        self.ids.victory_label.opacity = 0

    def go_to_welcome_screen(self):
        CoinbirdApp.game.current = "welcome_screen"
        
        
class HUD(MDBoxLayout):
    pass


class CoinbirdApp(MDApp):
    version = StringProperty("")
    
    def build(self):
        self.version = VERSION
        
        CoinbirdApp.game = Game(transition=FadeTransition())
        self.game.add_widget(WelcomeScreen(name="welcome_screen"))
        self.game.add_widget(GameScreen(name="game_screen"))
        self.game.add_widget(OptionsScreen(name="options_screen"))
        self.game.add_widget(ShopScreen(name="shop_screen"))
        return self.game


if __name__ == "__main__":
    LabelBase.register(
        name="Rabbit",
        fn_regular="fonts/Komika_display.ttf",
        fn_bold="fonts/Rubik-Bold.ttf",
        fn_italic="fonts/KomikaTitle-Paint.ttf",
    )
    CoinbirdApp().run()
