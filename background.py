from kivy.core.image import Image
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ObjectProperty


class BaseWidget(Widget):
    def load_tileable(self, name):
        t = Image(f"images/{name}.png").texture
        t.wrap = "repeat"
        setattr(self, f"tx_{name}", t)


class Background(BaseWidget):
    tx_floor = ObjectProperty(None)
    tx_grass = ObjectProperty(None)
    tx_cloud = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(Background, self).__init__(**kwargs)
        for name in ("floor", "grass", "cloud"):
            self.load_tileable(name)

    def set_background_size(self, tx):
        tx.uvsize = (self.width / tx.width, -1)

    def on_size(self, *args):
        for tx in (self.tx_floor, self.tx_grass, self.tx_cloud):
            self.set_background_size(tx)

    def set_background_uv(self, name, val):
        t = getattr(self, name)
        t.uvpos = ((t.uvpos[0] + val) % self.width, t.uvpos[1])
        self.property(name).dispatch(self)

    def update(self, nap):
        self.set_background_uv("tx_cloud", 0.2 * nap)
        self.set_background_uv("tx_grass", 0.4 * nap)
        self.set_background_uv("tx_floor", 2 * nap)
