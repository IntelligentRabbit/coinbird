from kivy.uix.screenmanager import Screen
from kivy.animation import Animation
from kivy.metrics import sp


class WelcomeScreen(Screen):
    def on_enter(self):
        self.animate_title_label()
        self.animate_play_btn()
        self.animate_options_btn()
        self.animate_shop_btn()
        
    def on_leave(self):
        self.ids.play_btn.pos_hint = {"center_x": .6, "top": .6}
        self.ids.options_btn.pos_hint = {"center_x": .4, "top": .4}
        self.ids.shop_btn.pos_hint = {"center_x": .6, "top": .2}
        
    def play_btn_press(self):
        self.game.current = "game_screen"
        self.game.sound_click.play()

    def options_btn_press(self):
        self.game.current = "options_screen"
        self.game.sound_click.play()

    def shop_btn_press(self):
        self.game.current = "shop_screen"
        self.game.sound_click.play()

    def animate_title_label(self):
        anim = Animation(font_size=sp(28), duration=0.2)
        anim += Animation(font_size=sp(26), duration=0.2)
        anim.start(self.ids.title_label)

    def animate_play_btn(self):
        anim = Animation(pos_hint={"center_x": .52, "top": .6}, duration=0.4)
        anim.start(self.ids.play_btn)
        
    def animate_options_btn(self):
        anim = Animation(pos_hint={"center_x": .52, "top": .4}, duration=0.4)
        anim.start(self.ids.options_btn)
        
    def animate_shop_btn(self):
        anim = Animation(pos_hint={"center_x": .52, "top": .2}, duration=0.4)
        anim.start(self.ids.shop_btn)
        